import Component from '@ember/component';
import { defineProperty, computed } from '@ember/object';
import EmberObject from '@ember/object';

function combineColumnAndRowData(additionalObservedProperties) {
  return Ember.computed('columns', 'columns.@each', 'row', ...additionalObservedProperties, function () {
    var row = this.get('row')
    return this.get('columns').map(function (column) {
      var object = EmberObject.extend({});
      var rowInput = row.get(column.get('input'));
      var control = object.create({
        "readOnly": rowInput.get("readOnly") ? rowInput.get("readOnly") : column.get("readOnly"),
        "type": rowInput.get("type") ? rowInput.get("type") : column.get("type"),
        "placeHolder": rowInput.get("placeHolder") ? rowInput.get("placeHolder") : column.get("placeHolder"),
        "content": rowInput.get("content") ? rowInput.get("content") : column.get("content"),
        "optionValuePath": rowInput.get("optionValuePath") ? rowInput.get("optionValuePath") : column.get("optionValuePath"),
        "optionLabelPath": rowInput.get("optionLabelPath") ? rowInput.get("optionLabelPath") : column.get("optionLabelPath"),
        "fixedDecimalPlaces": rowInput.get("fixedDecimalPlaces") ? rowInput.get("fixedDecimalPlaces") : column.get("fixedDecimalPlaces"),
        "requiresConfirmation": rowInput.get("requiresConfirmation") ? rowInput.get("requiresConfirmation") : column.get("requiresConfirmation"),
      });

      if (control.type !== "select" && control.type !== "multiselect" && (control.content || control.optionLabelPath || control.optionValuePath)) {
        control.set("hideSelect", true);
      }

      var returnObj = object.create({
        'control': control,
        'column': column,
        'row': rowInput
      });
      return returnObj;
    });
  })
}
export default Component.extend({
  rowPropertiesToObserve: null,
  init() {
    this._super(...arguments);
    var observedProperties = this.get("observedProperties");
    Ember.defineProperty(this, "combinedColumnData", combineColumnAndRowData(observedProperties))
  },
  tagName: 'tr',
  classNameBindings: ['additionalClasses'],
  additionalClasses: computed('row', function () {
    var row = this.get('row')
    return row.get('additionalClasses');
  }),

  unlockConfig: computed('row.unlockConfig', function () {
    return this.getWithDefault("row.unlockConfig", EmberObject.create({}))
  }),
  lockConfig: computed('row.lockConfig', function () {
    return this.getWithDefault("row.lockConfig", EmberObject.create({}))
  }),
  unlockConfirmationMessage: computed('unlockConfig.messageContent', function () {
    return this.getWithDefault("unlockConfig.messageContent", "Are you sure you want to unlock this row?")
  }),
  lockConfirmationMessage: computed('lockConfig.messageContent', function () {
    return this.getWithDefault("lockConfig.messageContent", "Are you sure you want to lock this row?")
  }),

  lockRowDialogName: computed('index', function () {
    var index = this.get('index');
    return "lockRowDialog" + index
  }),
  unlockRowDialogName: computed('index', function () {
    var index = this.get('index');
    return "unlockRowDialog" + index
  }),
  showLockDialog(){
    var dialogName = this.get("lockRowDialogName");
    $("#" + dialogName).modal("show");
  },
  showUnlockDialog(){
    var dialogName = this.get("unlockRowDialogName");
    $("#" + dialogName).modal("show");
  },
  actions: {
    clickLock() {
      var row = this.get("row");
      var isLocked = row.get('locked');
      if (this.get("unlockOnly") && !isLocked) {
        return;
      }

      if (row.get("lockRequiresConfirmation")) {
        if (isLocked) {
          this.showUnlockDialog();
        } else {
          this.showLockDialog();
        }
      } else {
        row.set('locked', !isLocked);
        this.onUpdate();
      }
    },
    updateRow(columnData){
      var key = columnData.get("column.input")
      this.onUpdate(this.get("index"), key, columnData.get("control.requiresConfirmation"));
    },
    updateRowDate(columnData, fromDate, toDate){
      var row = columnData.get("row")
      var key = columnData.get("column.input")
      row.set("value", toDate)
      this.onUpdate(this.get("index"), key, columnData.get("control.requiresConfirmation"));
    },
    afterConfirmUnlockRow(){
      var row = this.get("row");
      row.set('locked', false);
      this.onUpdate();
    },
    afterConfirmLockRow(){
      var row = this.get("row");
      row.set('locked', true);
      this.onUpdate();
    }
  },
});
